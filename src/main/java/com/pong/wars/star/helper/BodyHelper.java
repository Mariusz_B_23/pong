package com.pong.wars.star.helper;

import com.badlogic.gdx.physics.box2d.*;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;
import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.StaticBody;

public class BodyHelper {

    public static Body createBody(float x, float y, float width, float height,
                                  boolean isStatic, float density, World world,
                                  ContactType contactType) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = !isStatic ? DynamicBody : StaticBody;
        bodyDef.position.set(x / Const.PPM, y / Const.PPM);
        bodyDef.fixedRotation = true;
        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(width / 2 / Const.PPM, height / 2 / Const.PPM);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = density;
        body.createFixture(fixtureDef).setUserData(contactType);
        polygonShape.dispose();
        return body;
    }

}