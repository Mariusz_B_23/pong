package com.pong.wars.star.core;


import com.badlogic.gdx.physics.box2d.*;

import static com.pong.wars.star.helper.ContactType.*;

public class GameContactListener implements ContactListener {

    private final GameScreen gameScreen;

    public GameContactListener(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture a = contact.getFixtureA();
        Fixture b = contact.getFixtureB();

        if (a == null || b == null) {
            return;
        }
        if (a.getUserData() == null || b.getUserData() == null) {
            return;
        }
        if (a.getUserData() == BALL || b.getUserData() == BALL) {
            if (a.getUserData() == PLAYER || b.getUserData() == PLAYER) {
                gameScreen.getBall().reverseVelocityX();
                gameScreen.getBall().increaseSpeed();
            }
        }
        if (a.getUserData() == WALL || b.getUserData() == WALL) {
            gameScreen.getBall().reverseVelocityY();
        }
    }

    @Override
    public void endContact(Contact contact) {
    }

    @Override
    public void preSolve(Contact contact, Manifold manifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse contactImpulse) {
    }

}