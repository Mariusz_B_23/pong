package com.pong.wars.star.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.pong.wars.star.objects.*;
import org.lwjgl.opengl.GL20;

import static com.badlogic.gdx.Gdx.app;
import static com.badlogic.gdx.Gdx.input;
import static com.badlogic.gdx.Input.Keys.ESCAPE;
import static com.badlogic.gdx.Input.Keys.R;
import static com.pong.wars.star.core.Boot.getInstance;
import static com.pong.wars.star.core.Boot.instance;

public class GameScreen extends ScreenAdapter {

    private final OrthographicCamera orthographicCamera;
    private final SpriteBatch spriteBatch;
    private final World world;

    private final Player player;
    private final PlayerAI playerAI;
    private final Ball ball;
    private final WallTop wallTop;
    private final WallBottom wallBottom;
    private final TextureRegion[] numbers;


    public GameScreen(OrthographicCamera orthographicCamera) {
        this.orthographicCamera = orthographicCamera;
        this.orthographicCamera.position
                .set(new Vector3(getInstance().getScreenWidth() / 2,
                        getInstance().getScreenHeight() / 2, 0));
        this.spriteBatch = new SpriteBatch();
        this.world = new World(new Vector2(0, 0), false);
        this.player = new Player(16, instance.getScreenHeight() / 2, this);
        this.playerAI = new PlayerAI(getInstance().getScreenWidth() - 16,
                getInstance().getScreenHeight() / 2, this);
        this.ball = new Ball(this);
        this.wallTop = new WallTop(32, this);
        this.wallBottom = new WallBottom(getInstance().getScreenHeight() - 32, this);
        GameContactListener gameContactListener = new GameContactListener(this);
        this.world.setContactListener(gameContactListener);
        this.numbers = loadTextureSprite();
    }


    public void update() {
        world.step(1 / 60f, 6, 2);
        this.player.update();
        this.playerAI.update();
        this.orthographicCamera.update();
        this.ball.update();
        spriteBatch.setProjectionMatrix(orthographicCamera.combined);

        if (input.isKeyPressed(ESCAPE)) {
            app.exit();
        }
        if (input.isKeyJustPressed(R)) {
            this.ball.reset();
            this.player.setScore(0);
            this.playerAI.setScore(0);
        }
    }

    @Override
    public void render(float delta) {
        update();
        Texture background = new Texture("deathstar.png");
        Sprite backgroundSprite = new Sprite(background);
        backgroundSprite.setSize(getInstance().getScreenWidth(), getInstance().getScreenHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.begin();
        backgroundSprite.draw(spriteBatch);
        this.player.render(spriteBatch);
        this.playerAI.render(spriteBatch);
        this.ball.render(spriteBatch);
        this.wallTop.render(spriteBatch);
        this.wallBottom.render(spriteBatch);
        this.drawNumbers(spriteBatch, (int) player.getScore(), 0, getInstance().getScreenHeight() - 755);
        this.drawNumbers(spriteBatch, (int) playerAI.getScore(), getInstance().getScreenWidth() - 48, getInstance().getScreenHeight() - 55);
        spriteBatch.end();

//        this.box2DDebugRenderer.render(world, orthographicCamera.combined.scl(PPM));
    }

    private void drawNumbers(SpriteBatch spriteBatch, int number, float x, float y) {
        if (number < 10) {
            spriteBatch.draw(numbers[number], x, y, (float) 30, (float) 42);
        } else {
            spriteBatch.draw(numbers[Integer.parseInt(("" + number).substring(0, 1))], x, y, (float) 30, (float) 42);
            spriteBatch.draw(numbers[Integer.parseInt(("" + number).substring(1, 2))], x + 20, y, (float) 30, (float) 42);
        }
    }

    TextureRegion[] loadTextureSprite() {
        Texture texture = new Texture("numbers.png");
        return TextureRegion.split(texture, texture.getWidth() / 10, texture.getHeight())[0];
    }

    public World getWorld() {
        return world;
    }

    public Ball getBall() {
        return ball;
    }

    public Player getPlayer() {
        return player;
    }

    public PlayerAI getPlayerAI() {
        return playerAI;
    }

}