package com.pong.wars.star.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.pong.wars.star.core.GameScreen;

import static com.pong.wars.star.core.Boot.getInstance;
import static com.pong.wars.star.helper.BodyHelper.createBody;
import static com.pong.wars.star.helper.Const.PPM;
import static com.pong.wars.star.helper.ContactType.BALL;

public class Ball {

    private final Body body;
    private float x;
    private float y;
    private float speed;
    private float velX;
    private float velY;
    private final float width;
    private final float height;
    private final Texture texture;
    private final GameScreen gameScreen;
    private float angle;

    public Ball(GameScreen gameScreen) {
        this.x = getInstance().getScreenWidth() / 2;
        this.y = getInstance().getScreenHeight() / 2;
        this.speed = 10;
        this.velX = getRandomDirection();
        this.velY = getRandomDirection();
        this.texture = new Texture("ball.png");
        this.width = 64;
        this.height = 64;
        this.gameScreen = gameScreen;
        this.body = createBody(x, y, width, height, false, 0, gameScreen.getWorld(), BALL);
        this.angle = 0;
    }

    private float getRandomDirection() {
        return (Math.random() < 0.5) ? 1 : -1;
    }

    public void update() {
        x = body.getPosition().x * PPM - (width / 2) + 35;
        y = body.getPosition().y * PPM - (height / 2);
        this.body.setLinearVelocity(velX * speed, velY * speed);

        if (x < 0) {
            gameScreen.getPlayerAI().score();
            reset();
        }
        if (x > getInstance().getScreenWidth()) {
            gameScreen.getPlayer().score();
            reset();
        }

        angle += 5;
        angle %= 360;
    }

    public void reset() {
        this.velX = getRandomDirection();
        this.velY = getRandomDirection();
        this.speed = 10;
        this.body.setTransform(getInstance().getScreenWidth() / 2 / PPM,
                getInstance().getScreenHeight() / 2 / PPM, 0);
        this.angle = 0;
    }

    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, x, y, width / 2, height / 2, width, height, 1, 1,
                angle, 0, 0, texture.getWidth(), texture.getHeight(), false, false);
    }

    public void reverseVelocityX() {
        this.velX *= -1;
    }

    public void reverseVelocityY() {
        this.velY *= -1;
    }

    public void increaseSpeed() {
        this.speed *= 1.4f;
    }

    public float getY() {
        return y;
    }

}