package com.pong.wars.star.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.pong.wars.star.core.GameScreen;

import static com.badlogic.gdx.Input.Keys.*;
import static com.pong.wars.star.helper.Const.PPM;

public class Player extends PlayerPaddle {

    public Player(float x, float y, GameScreen gameScreen) {
        super(x, y, gameScreen);
        this.texture = new Texture("vader.png");
    }

    public void update() {
        x = body.getPosition().x * PPM - (width / 2) + 35;
        y = body.getPosition().y * PPM - (height / 2);
        velY = 0;

        if (Gdx.input.isKeyPressed(W)) {
            velY = 1;
        }
        if (Gdx.input.isKeyPressed(S)) {
            velY = -1;
        }

        body.setLinearVelocity(0, velY * speed);
    }

}