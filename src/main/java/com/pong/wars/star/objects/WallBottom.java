package com.pong.wars.star.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.pong.wars.star.core.Boot;
import com.pong.wars.star.helper.BodyHelper;
import com.pong.wars.star.helper.ContactType;
import com.pong.wars.star.core.GameScreen;

public class WallBottom {

    private final float x;
    private final float y;
    private final float width;
    private final float height;
    private final Texture texture;


    public WallBottom(float y, GameScreen gameScreen) {
        this.x = Boot.getInstance().getScreenWidth() / 2;
        this.y = y;
        this.width = Boot.getInstance().getScreenWidth();
        this.height = 50;
        this.texture = new Texture("red right.png");
        Body body = BodyHelper.createBody(x, y, width, height, true, 0, gameScreen.getWorld(), ContactType.WALL);
    }

    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, x - (width /2), y - (height /2), width, height);
    }
}