package com.pong.wars.star.objects;

import com.badlogic.gdx.graphics.Texture;
import com.pong.wars.star.helper.Const;
import com.pong.wars.star.core.GameScreen;

public class PlayerAI extends PlayerPaddle {


    public PlayerAI(float x, float y, GameScreen gameScreen) {
        super(x, y, gameScreen);
        this.texture = new Texture("yoda.png");
    }


    public void update() {
        x = body.getPosition().x * Const.PPM - (width / 2) - 35;
        y = body.getPosition().y * Const.PPM - (height / 2);
        velY = 0;

        Ball ball = gameScreen.getBall();
        if (ball.getY() + 10 > y && ball.getY() - 10 > y) {
            velY = 1;
        }
        if (ball.getY() + 10 < y && ball.getY() - 10 < y) {
            velY = -1;
        }

        body.setLinearVelocity(0, velY * speed);
    }

}