package com.pong.wars.star.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.pong.wars.star.core.GameScreen;

import static com.pong.wars.star.core.Boot.getInstance;
import static com.pong.wars.star.helper.BodyHelper.createBody;
import static com.pong.wars.star.helper.ContactType.WALL;

public class WallTop {

    private final float x;
    private final float y;
    private final float width;
    private final float height;
    private final Texture texture;


    public WallTop(float y, GameScreen gameScreen) {
        this.x = getInstance().getScreenWidth() / 2;
        this.y = y;
        this.width = getInstance().getScreenWidth();
        this.height = 50;
        this.texture = new Texture("red left.png");
        Body body = createBody(x, y, width, height, true, 0, gameScreen.getWorld(), WALL);
    }

    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, x - (width /2), y - (height /2), width, height);
    }
}