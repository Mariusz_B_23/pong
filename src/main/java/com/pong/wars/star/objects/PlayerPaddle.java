package com.pong.wars.star.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.pong.wars.star.core.GameScreen;
import com.pong.wars.star.helper.BodyHelper;
import com.pong.wars.star.helper.ContactType;

public abstract class PlayerPaddle {

    protected Body body;
    protected float x;
    protected float y;
    protected float speed;
    protected float velY;
    protected float width;
    protected float height;
    protected float score;
    protected Texture texture;
    protected GameScreen gameScreen;


    public PlayerPaddle(float x, float y, GameScreen gameScreen) {
        this.x = x;
        this.y = y;
        this.gameScreen = gameScreen;
        this.speed = 10;
        this.width = 100;
        this.height = 150;
        this.body = BodyHelper.createBody(x, y, width, height, false,
                10_000, gameScreen.getWorld(), ContactType.PLAYER);
    }

    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, x, y, width, height);
    }

    public void score() {
        this.score++;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

}